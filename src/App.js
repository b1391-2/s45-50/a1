// Delete this : import logo from './logo.svg';
// Delet also this : import './App.css';

//Component
import AppNavbar from './components/AppNavbar';

import { Container } from 'react-bootstrap';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { useState, useEffect } from 'react';

//Pages
import Home from './pages/Home';
import Course from './pages/Course';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import CourseView from './components/CourseView';

//for react User Context 
import { UserProvider } from './UserContext';

//Wrap JSX element in an enclosing tag
//import { Fragment } from 'react';

export default function App() {

  // React Context is nothing but a global state to the app. It is a way to make a particular data available to all the components no matter how they are nested

  const [user, setUser] = useState({
    id: null,
    email: null
  })

  //Function for clearing localStorage on Logout
  const unsetUser = () =>{
    localStorage.clear()
    setUser({
      id: null,
      isAdmin: null
    })
  }

  useEffect(() => {
    fetch("https://ayeah-ecommerce-booking.herokuapp.com/ecommerce/users/login", {
      method: "GET",
      headers: {
        "Authorization": `Bearer ${localStorage.getItem("token")}`
      }
    }).then(response => response.json()).then(result => {
      if (typeof result._id !== undefined) {
        //console.log(`if`);
        setUser({
          id: result._id,
          isAdmin: result.isAdmin
        })
      }
    })
  }, [])

  console.log(user)

  return (
      <UserProvider value={{user, setUser, unsetUser}}>
        <BrowserRouter>
          <AppNavbar/>
          <Container>
          <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/courses" component={Course} />
              <Route exact path="/register" component={Register} />
              <Route exact path="/courses/:courseId" component={CourseView} />
              <Route exact path="/login" component={Login} />
              <Route exact path="/logout" component={Logout} />
              <Route component={Error}/>
          </Switch>
          </Container>
        </BrowserRouter>
      </UserProvider>
    )
}