
import { useState, useEffect } from 'react';
import { Container, Button, Row, Col, Table, Modal , Form} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AdminView (props) {
	//console.log(props);

	const { courseData, fetchData } = props
	//console.log(courseData); 
	//console.log(fetchData);

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [courseID, setCourseID] = useState("")
	const [courses, setCourses] = useState([])

	//Add Show Button
	const [showAdd, setShowAdd] = useState(false);
	const openAdd = () => setShowAdd(true)
	const closeAdd = () => setShowAdd(false)

	//Show Edit 
	const [showEdit, setShowEdit] = useState(false);
	const openEdit = () => setShowEdit(true);
	const closeEdit = () => setShowEdit(false)

  	useEffect(() => {
  		fetchData()
		const courseArr = courseData.map((course) => {
		//console.log(course.isActive)
		
		return (
			<tr>
				<td>{course.courseName}</td>
				<td>{course.description}</td>
				<td>{course.price}</td>
				<td>{
					(course.isActive === true) ?
						<span>Available</span>
					:
						<span>Unavailable</span>
				}</td>
				<td>
					<Button onClick={(e) => {
						retrieveCourse(course._id)
						
					}}>Update</Button>
					{
						(course.isActive) ?
							<Button variant="warning" onClick={() => {
								archiveCourse(course._id)}}>Disable</Button>
						:
							<Button variant="success" onClick={() => {
								unarchiveCourse(course._id)}}>Enable</Button>
					}
					<Button variant="danger">Delete</Button>
				</td>
			</tr>
		)
		
	})
		setCourses(courseArr);
	},[courseData])

	const addCourse = (e) => {
		e.preventDefault()

		fetch("https://ayeah-booking.herokuapp.com/api/courses/create-course", {
			method: "POST",
			headers: {
				"Content-Type" : "application/json",
				"Authorization" : `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				courseName: name,
				description: description,
				price: price
			})
		}).then(response => response.json()).then(response => {
			console.log(response);

			if (response) {
				Swal.fire({
					title: "Success!",
					icon :"success",
					text: "Course Added Successfully"
				})

				closeAdd()
				fetchData()
			} else {
				Swal.fire({
					title: "Got some Error",
					icon : "error",
					text: "Something went wrong. Please Try again."
				})
				fetchData()
			}
		})
	}

	const retrieveCourse = (courseId) => {
		fetch(`https://ayeah-booking.herokuapp.com/api/courses/${courseId}`, {
			method: "GET",
			headers: {
				"Authorization" : `Bearer ${localStorage.getItem("token")}`
			}
		}).then(response => response.json()).then(response => {
			//console.log( response)
			if (response) {

				openEdit()
				setCourseID(response._id)
				setName(response.courseName);
				setDescription(response.description);
				setPrice(response.price);
			}
		})
	}

	const editCourse = (e) => {
		e.preventDefault()
		//console.log(courseID)

		fetch(`https://ayeah-booking.herokuapp.com/api/courses/${courseID}/update-course`, {
			method: "PUT",
			headers: {
				"Content-Type" : "application/json",
				"Authorization" : `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				courseName: name,
				description: description,
				price: price
			})
		}).then(response => response.json()).then(response => {
			//console.log(response);

			if (response) {
				Swal.fire({
					title: "Success!",
					icon :"success",
					text: "Course Editted Successfully"
				})

				closeEdit()
				setName("");	
				setDescription("");
				setPrice("");
				fetchData();
			} else {
				Swal.fire({
					title: "Got some Error",
					icon : "error",
					text: "Something went wrong. Please Try again."
				})
				fetchData();

			}
		})
	}

	const archiveCourse = (courseId) => {

		fetch(`https://ayeah-booking.herokuapp.com/api/courses/${courseId}/archive`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		}).then(response => response.json()).then(response => {
			//console.log(response);
			if(response === true){
					fetchData()
					Swal.fire({
						title: "Success",
						icon: "success",
						text: "Course Disabled"
					})				
				} else {
					fetchData()
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again."
					})

				}
		})
	}

	const unarchiveCourse = (courseId) => {
		fetch(`https://ayeah-booking.herokuapp.com/api/courses/${courseId}/unarchive`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization" : `Bearer ${localStorage.getItem("token")}`
			}
		}).then(response => response.json()).then(response => {
			//console.log(response);
			if(response === true){
					fetchData()
					Swal.fire({
						title: "Success",
						icon: "success",
						text: "Course Enabled"
					})
				} else {
					fetchData()
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again."
					})
				}

			
		})
	}
	//console.log(courseID);
	return (
		<Container>
			<h2 className="text-center">Admin Dashboard</h2>
			<Row>
				<Col>
					<div className="text-right">
						<Button onClick={openAdd}>Add New Course</Button>
					</div>
				</Col>
			</Row>
			<Table striped bordered hover>
				<thead>
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{courses}
				</tbody>
			</Table>


		{/*Add Course Modal*/}
		<Modal show={showAdd} onHide={closeAdd}>
			<Modal.Header closeButton>
				<Modal.Title>Add Course</Modal.Title>
			</Modal.Header>
			<Modal.Body>
				<Form onSubmit={ (e) => {
					addCourse(e)
				}}>
					<Form.Group controlId="courseName">
						<Form.Label>Course Name:</Form.Label>
						<Form.Control 	type="text"
										value={name}
										onChange={(event) => {
											setName(event.target.value);
										}}/>
					</Form.Group>
					<Form.Group controlId="courseDescription">
						<Form.Label>Description:</Form.Label>
						<Form.Control 	type="text"
										value={description}
										onChange={(event) => {
											setDescription(event.target.value);
										}}/>
					</Form.Group>
					<Form.Group controlId="price">
						<Form.Label>Price:</Form.Label>
						<Form.Control 	type="number"
										value={price}
										onChange={(event) => {
											setPrice(event.target.value);
										}}/>
					</Form.Group>
					<Button variant="success" type="submit">Submit</Button>
					<Button variant="secondary" onClick={closeAdd}>Close</Button>
				</Form>
			</Modal.Body>
		</Modal>

	{/*Edit Course Modal*/}

	<Modal show={showEdit} onHide={closeEdit}>
		<Modal.Header closeButton>
			<Modal.Title>Edit Course</Modal.Title>
		</Modal.Header>
		<Modal.Body>
			<Form onSubmit={ (e) => {
				editCourse(e)
			}}>
				<Form.Group controlId="courseName">
					<Form.Label>Course Name: </Form.Label>
					<Form.Control 	type="text"
									value={name}
									onChange={(event) => {
										setName(event.target.value);
									}}/>
				</Form.Group>
				<Form.Group controlId="courseDescription">
					<Form.Label>Description:</Form.Label>
					<Form.Control 	type="text"
									value={description}
									onChange={(event) => {
										setDescription(event.target.value);
									}}/>
				</Form.Group>
				<Form.Group controlId="price">
					<Form.Label>Price:</Form.Label>
					<Form.Control 	type="number"
									value={price}
									onChange={(event) => {
										setPrice(event.target.value);
									}}/>
				</Form.Group>
				<Button variant="success" type="submit">Submit</Button>
				<Button variant="secondary" onClick={closeEdit}>Close</Button>
			</Form>
		</Modal.Body>
	</Modal>
		</Container>
	)
}