import { Navbar, Container, Nav, NavDropdown } from 'react-bootstrap';

import { NavLink } from 'react-router-dom';

import { useContext } from 'react';

import UserContext from './../UserContext';

//Import the Bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css'; 



export default function AppNavbar () {

	//const [user ] = useState(localStorage.getItem("email"));
	//console.log(user)
	//let history = useHistory()

	const { user } = useContext(UserContext);
	//console.log(user);
	
	let loginNav = (user.id !== null) ?
		<NavDropdown.Item as={NavLink} to="/logout">Logout</NavDropdown.Item>
	:
		<NavDropdown.Item as={NavLink} to="/login">Login</NavDropdown.Item> 

	let registerNav = (user.id !== null) ?
		null
	:
		<NavDropdown.Item as={NavLink} to="/register">Register</NavDropdown.Item>


	return (
		<Navbar bg="primary" expand="lg">
		  <Container>
		    <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav">
		      <Nav className="me-auto">
		        <Nav.Link as={NavLink} to="/">Home</Nav.Link>
		        <Nav.Link as={NavLink} to="/courses">Course</Nav.Link>
		        <NavDropdown title="Dropdown" id="basic-nav-dropdown">
		        <Nav>
		        	{registerNav}
		        </Nav>  
		          <NavDropdown.Divider />
		        <Nav>
		        	{loginNav}
		        </Nav>

		        </NavDropdown>
		      </Nav>
		    </Navbar.Collapse>
		  </Container>
		</Navbar>
	)
}
