import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner ({data}) {
	//console.log(data)

	const { title, content, destination, label } = data


	return (
		<div className="container-fluid">
			<div className="row justify-content-center mt-3">
				<div className="col-10 col-md-8">
					{/*Jumbotron*/}
					<div className="jumbotron">
						<h1>{title}</h1>
						<div>{content}</div>
						<Link to={destination}><Button className="button btn-primary">{label}</Button></Link>
					</div>
				</div>
			</div>
		</div>
	)
}