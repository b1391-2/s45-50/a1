import { Container, Row, Col, Card } from "react-bootstrap";

export default function Highlights () {
	return (
		/*Card*/

		<Container fluid>
			<Row>
				<Col className="d-flex mb-3">
					<Card xs={12} md={4} className="cardHighlights p-3 m-3">
					  <Card.Body>
					    <Card.Title>Learn from Home</Card.Title>
					    <Card.Text>
					       Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur neque provident facere, maxime fugiat incidunt! Debitis aspernatur sit, ullam exercitationem delectus dolor quaerat libero accusantium nostrum officia, autem quia nesciunt?
					    </Card.Text>
					  </Card.Body>
					</Card>
					<Card xs={12} md={4} className="cardHighlights p-3 m-3">
					  <Card.Body>
					    <Card.Title>Study Now Pay Later</Card.Title>
					    <Card.Text>
					    Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur neque provident facere, maxime fugiat incidunt! Debitis aspernatur sit, ullam exercitationem delectus dolor quaerat libero accusantium nostrum officia, autem quia nesciunt?
					    </Card.Text>
					  </Card.Body>
					</Card>
					<Card xs={12} md={4} className="cardHighlights p-3 m-3">
					  <Card.Body>
					    <Card.Title>Be a Part of  our Community</Card.Title>
					    <Card.Text>
					       Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur neque provident facere, maxime fugiat incidunt! Debitis aspernatur sit, ullam exercitationem delectus dolor quaerat libero accusantium nostrum officia, autem quia nesciunt?
					    </Card.Text>
					  </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>


	)
}