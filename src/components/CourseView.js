import { Col, Row, Card, Container, Button } from 'react-bootstrap';
import { useEffect, useState, useContext } from 'react';
import { useParams, useHistory, Link } from 'react-router-dom';
import Swal from 'sweetalert2';

import UserContext from './../UserContext';

export default function CourseView () {
	const {user, setUser} = useContext(UserContext)
	const [courseName, setCourseName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const {courseId} = useParams()
	//console.log(courseId);
	
	let history = useHistory()

	useEffect(() => {
		fetch(`https://ayeah-booking.herokuapp.com/api/courses/${courseId}`, {
			method: "GET",
			headers: {
				"Authorization" : `Bearer ${localStorage.getItem("token")}`
			}
		}).then(response => response.json()).then(data => {
			//console.log(data);
			setCourseName(data.courseName);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [courseId])


	const enroll = (courseId) => {
		fetch("https://ayeah-booking.herokuapp.com/api/users/enroll", {
			method: "POST",
			headers: {
				"Content-Type" : "application/json",
				"Authorization" : `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				courseId: courseId
			})
		}).then(response => response.json()).then(result => {
			console.log(result);
			if (result === true) {
				Swal.fire({
					title: "Successfully Enrolled!",
					icon: "success",
					text: "You have successfully enrolled for this course"
				})

				history.push("/courses")
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
			}
		})
	}


	return(
		<Container>
			<Row className="justify-content-center p-4">
				<Col xs={8} md={4}>
					<Card className="p-4">
						<Card.Title>{courseName}</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Text>Price:</Card.Text>
						<Card.Text>{price}</Card.Text>
						<Card.Subtitle>Class Schedule</Card.Subtitle>
						<Card.Text>8AM - 5PM</Card.Text>
						{
							(user.id !== null) ? 
							<Button onClick={() => {enroll(courseId)}}>Enroll</Button>
							:
							<Link className="btn btn-primary" to="/login">Enroll Now!</Link>
						}
					</Card>
				</Col>
			</Row>
		</Container>
	)
}