
import CourseCard from './CourseCard';
import {useState, useEffect, Fragment} from 'react';

export default function UserView({courseData}) {

	const [courses, setCourses] = useState([]);


	//console.log(courseData);
	useEffect(() => {
		const activeCourses = courseData.map((course) => {
			//console.log(course)

			if(course.isActive === true) {
				return (
					<CourseCard 
						courseProperty={course}
						key={course._id}
					/>
				)
			} else {
				return null
			}


		
		})

		setCourses(activeCourses);
	}, [courseData])

	//console.log(courses);


	return (
	<Fragment>
		{courses}
	</Fragment>	
	)
}