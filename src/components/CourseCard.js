import { Card, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from './../UserContext';
import { Link } from 'react-router-dom';


export default function CourseCard ({courseProperty}) {
	const { user, setUser } = useContext(UserContext);

	//console.log(user.id)
	//console.log(courseProperty)

	const { _id, courseName, description, price } = courseProperty

	return (
		
		<Card className="m-5 p-3">
		  <Card.Body>
		    <Card.Title>{courseName}</Card.Title>
		    <Card.Subtitle>Course Description:</Card.Subtitle>
		    <Card.Text>{description}</Card.Text>
		    <Card.Subtitle>Price:</Card.Subtitle>
		    <Card.Text>{price}</Card.Text>
		    <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
		  </Card.Body>
		</Card>
	)
}

