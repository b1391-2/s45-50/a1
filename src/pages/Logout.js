import { Redirect } from 'react-router-dom';
import UserContext from './../UserContext';
import { useContext, useEffect } from 'react';

export default function Logout () {
	
	const { user, setUser, unsetUser } = useContext(UserContext);

	unsetUser();
	
	useEffect(() => {
		setUser({
			id: null,
			email: null
		})
	}, user)

	return (

		<Redirect to="/login" />
	)
}