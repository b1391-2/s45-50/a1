//import coursesData from './../data/coursesData';

//Import the component
import CourseCard from './../components/CourseCard';

import { useEffect, useState, Fragment, useContext } from 'react';
import UserContext from './../UserContext';


import AdminView from './../components/AdminView';
import UserView from './../components/UserView';

export default function Course () {

	//console.log(coursesData);
	const { user, setUser } = useContext(UserContext);

	//console.log(typeof user);
	//console.log(user); 

	const [courses, setCourses] = useState([]);
	//console.log(courses);

	const fetchData = () => {
		fetch("https://ayeah-booking.herokuapp.com/api/courses/").then(response => response.json()).then(result => {
		//console.log(result);
		setCourses(result);
	})
	}


	useEffect(() => { 
		fetchData();	
	}, [])

	const courseList = courses.map((course) => {
		//console.log(course)

		return (
			<CourseCard courseProperty={course}/>
		)
	})
	
	//console.log(user);	

	return (
		<Fragment>
			{
				(user.isAdmin === true) ?
					<AdminView courseData={courses} fetchData={fetchData}/>
				:
					<UserView courseData={courses}/>
			}
		</Fragment>
			
	)

}



	