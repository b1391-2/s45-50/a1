import Banner from './../components/Banner';

export default function Error () {

	const data = {
		title: "404 - Not Found",
		content: "The Page you are looking cannot be found.",
		destination: "/",
		label: "Back Home"
	}

	return( 
		<Banner data={data} />
	)
}