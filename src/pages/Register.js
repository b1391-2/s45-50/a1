import { Container, Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from './../UserContext';
import Swal from 'sweetalert2';
import { Redirect } from 'react-router-dom';


export default function Register () {

	const { user } = useContext(UserContext);
	//console.log(user);

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");
	const [isRegisteredSuccess, setIsRegisteredSuccess] = useState(false);

	const [isDisabled, setIsDisabled] = useState(true);
	//console.log(isRegisteredSuccess)
	useEffect( () => {
		if ((email !== "" && password !== "" && confirmPassword !== "") && (password === confirmPassword)) {
			setIsDisabled(false)

		} else {
			setIsDisabled(true)
		}
	}, [email, password, confirmPassword])

	function Register (event) {
		//console.log(email)
		event.preventDefault()

		//Check first if email exists
		fetch(`${process.env.REACT_APP_API_URL}/api/users/email-exists`,{
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		}).then(response => response.json()).then(result =>{
			console.log(result);
			if (result === false) {
				//console.log('if')
				Swal.fire({
					title: "Duplicate email found",
					icon: "error",
					text: "Please provide a different e-mail"
				})
			} else {
				//console.log('else')
				fetch("https://ayeah-booking.herokuapp.com/api/users/register", {
					method: "POST",
					headers: {
						"Content-Type" : "application/json"
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						mobileNo: mobileNo,
						email: email,
						password: password
					})
				}).then(response => response.json()).then(result => {
					console.log(result);
					if (result === true ) {
						Swal.fire({
					title: "User Registered Successfully!",
					icon: "success",
					text: "Welcome to Zuitt!"

				})	
					setIsRegisteredSuccess(true);

					} else {
						Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
					}
				})
			}
		})

		setFirstName("");
		setLastName("");
		setMobileNo("");
		setEmail("");
		setPassword("");
		setConfirmPassword("");
	}

	return (

		(isRegisteredSuccess === true) ?
		<Redirect to="/login" />
		 :
		<Container>
			<Form 	className="border p-5 mb-3"
					
			>
			{/*First Name*/}
			<Form.Group className="mb-3" controlId="fomFirstName">
			    <Form.Label>First Name</Form.Label>
{/*Input field*/}<Form.Control 	type="text" 
								placeholder="Enter First Name"
								value={firstName}
								onChange={ (event) => {
									//console.log(event.target.value)
									setFirstName(event.target.value)
								} } />
			 </Form.Group>

			{/*Last Name*/}
			<Form.Group className="mb-3" controlId="formLastName">
			    <Form.Label>First Name</Form.Label>
{/*Input field*/}<Form.Control 	type="text" 
								placeholder="Enter Last Name"
								value={lastName}
								onChange={ (event) => {
									//console.log(event.target.value)
									setLastName(event.target.value)
								} } />
			 </Form.Group>

			 {/*Mobile Number*/}
			<Form.Group className="mb-3" controlId="formMobileNumber">
			    <Form.Label>Mobile Number</Form.Label>
{/*Input field*/}<Form.Control 	type="text" 
								placeholder="Enter Number"
								value={mobileNo}
								onChange={ (event) => {
									//console.log(event.target.value)
									setMobileNo(event.target.value)
								} } />
			 </Form.Group>

			{/*Email*/}
			  <Form.Group className="mb-3" controlId="formBasicEmail">
			    <Form.Label>Email address</Form.Label>
{/*Input field*/}<Form.Control 	type="email" 
								placeholder="Enter email"
								value={email}
								onChange={ (event) => {
									//console.log(event.target.value)
									setEmail(event.target.value)
								} } />
			    <Form.Text className="text-muted">
			      We'll never share your email with anyone else.
			    </Form.Text>
			  </Form.Group>

			{/*Password*/}
			  <Form.Group className="mb-3" controlId="Password">
			    <Form.Label>Password</Form.Label>
{/*Input field*/}<Form.Control 	type="password"
								placeholder="Password"
								value={password}
								onChange={ (event) => {
									//console.log(event.target.value)
									setPassword(event.target.value)
								} }
								 />
			  </Form.Group>
			 

			 {/*onfirm Password*/}
			 <Form.Group className="mb-3" controlId="ConfirmPassword">
			   <Form.Label>Confirm Password</Form.Label>
{/*Input field*/}<Form.Control 	type="password" 
								placeholder="Confirm Password"
								value={confirmPassword}
								onChange={ (event) => {
									//console.log(event.target.value)
									setConfirmPassword(event.target.value)
								} } />
			 </Form.Group>

			  <Button 	variant="primary" 
			  			type="submit"
			  			disabled={isDisabled}
			  			onClick={ (event) => Register(event)}
						>
			    Submit
			  </Button>
			</Form>
		</Container>
	)
}