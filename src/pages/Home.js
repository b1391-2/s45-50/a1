import { Fragment } from 'react';

//Components
import Banner from './../components/Banner';
import Highlights from './../components/Highlights';

export default function Home () {

	const data = {
			title: "Zuitt Coding Bootcamp",
			content: "Opportunities for everone, everywhere!",
			destination: "/courses",
			label: "Enroll Now"
		}

	return (
		<Fragment>
			<Banner data={data}/>
			<Highlights />
		</Fragment>
	)
}