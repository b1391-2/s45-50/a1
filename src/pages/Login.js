import { useEffect, useState, useContext } from 'react';
//useContext is used to unpack the data from the UserContext

import UserContext from './../UserContext';
import { Container, Form, Button } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import Swal from 'sweetalert2';


export default function Login () {
	

	const { user, setUser } = useContext(UserContext); 

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const [isDisabled, setIsDisabled] = useState(true);

	useEffect( () => {
		if (email !== "" && password !== "") {
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}
	}, [email, password])

	function logIn (event) {
		event.preventDefault()

		fetch("https://ayeah-booking.herokuapp.com/api/users/login",{
			method: "POST",
			headers: {
				"Content-Type" : "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}).then(response => response.json()).then(result => {
				//console.log(result);
				//console.log(result.access)
				
				if (result !== "undefined"){
								//store the data in local storage
								localStorage.setItem("token", result.access)
								userDetails(result.access)

								//Alert the user that login is successful
								Swal.fire({
									title: "Login Successful",
									icon: "success",
									text: "Welcome to Course Booking!"
								})
							} else {
								Swal.fire({
									title: "Authentication Failed",
									icon: "error",
									text: "Check your login details and try again!"
								})
							}
			})

		setEmail("");
		setPassword("");
		//window.location.replace("http://localhost:3000/courses");


	}

	const userDetails = (token) => {
		fetch("https://ayeah-booking.herokuapp.com/api/users/details", {
			method: "GET",
			headers: ({
							"Authorization" : `Bearer ${token}`
						})
		}).then(response => response.json()).then(result => {
			//console.log(result);
			setUser({
				id: result._id,
				isAdmin: result.isAdmin
			});
		})
	}

	//console.log(user)
	return(

		(user.id != null) ?
		<Redirect to="/courses"/>
		:
		<Container className="p-4">
			<Form className="border p-5 mb-5">
			  <Form.Group className="mb-3" controlId="email">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control 	type="email"
			    				placeholder="Enter email"
			    				value={email}
			    				onChange={ (event) => {
			    					setEmail(event.target.value)
			    				}} />
			  </Form.Group>

			  <Form.Group className="mb-3" controlId="password">
			    <Form.Label>Password</Form.Label>
			    <Form.Control 	type="password" 
			    				placeholder="Password"
			    				value={password}
			    				onChange={ (event) => {
			    					setPassword(event.target.value)
			    				}} />
			  </Form.Group>
			  <Form.Group className="mb-3" controlId="formBasicCheckbox">
			    <Form.Check type="checkbox" label="Check me out" />
			  </Form.Group>
			  <Button 	variant="primary" 
			  			type="submit"
			  			disabled={isDisabled}
			  			onClick={ (event) => {
			  				//console.log(event)
			  				logIn(event)
			  			}}
			  			>
			    Submit
			  </Button>
			</Form>
		</Container>
	)
}